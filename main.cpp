#include <iostream>
#include <time.h>
#include "matrixlib.h"
#include <complex>
#include <cmath>

using namespace std::complex_literals;
int main(){

	clock_t Start,T1,T2,T3,End;
	matrix<int,3,3> m1(1,0,0,0,2,0,0,0,3);
	matrix<double,3,3> m2(2.1,0.0,0.0,0.0,2.2,0.0,0.0,0.0,2.3);
	matrix<double,3,3> m3;
	matrix<std::complex<int>,1,3 > m4((1,1),(2,2),(3,3));
	matrix<std::complex<int>,3,1 > m5((1,1),(2,2),(3,3));
	matrix<std::complex<int>,1,1 > m6;
	
	Start = clock();
	m2 *= m1;
	T1 = clock();
	m3 = (m1+m2)*(m1-m2);
	T2 = clock();
	m3 += m1+m2;
	T3 = clock();
	m6 = m4*m5;
	End = clock();
	
	for(int i=0;i<3;i++){
		for(int j =0;j<3;j++){
			std::cout<<m3(i,j)<<"\t";
		}
		std::cout<<"\n";
	}
	std::cout<<"\n"<<m6(0,0)<<"\n\n";

	std::cout<<"\ntime taken for doing m2*=m1: "<<(double)(T1-Start)/CLOCKS_PER_SEC<<" seconds";
	std::cout<<"\ntime taken for doing m3=(m1+m2)*(m1-m2): "<<(double)(T2-T1)/CLOCKS_PER_SEC<<" seconds";
	std::cout<<"\ntime taken for doing m3+=m1+m2: "<<(double)(T3-T2)/CLOCKS_PER_SEC<<" seconds";
	std::cout<<"\ntime taken for doing m6=m4*m5: "<<(double)(End-T3)/CLOCKS_PER_SEC<<" seconds\n";

	return 0;
}