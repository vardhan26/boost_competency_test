//the actual library
#ifndef MATRIX_H
#define MATRIX_H

//I haven't included any header files to fit everything in this file only

template<class DERIVED>
class Exp{
	public:
		inline const DERIVED& self(void) const{
			return static_cast<const DERIVED&>(*this);
		}

};

/* the matrix class. 
	a matrix here is stored as a single array with an element at (i,j) being at (i*<number of columns> + j)
	the "=" operator is overloaded so that a matrix is evaluated at its right hand side when it is encountered but not before
	I was initially going to use vectors but it seemed like a bit of an overkill- especially in terms of memory */

template<class elementType,unsigned rows_,unsigned cols_ >
class matrix : public Exp<matrix<elementType,rows_,cols_>>{  
public:
	matrix(void){}	// default constructor for matrix

	template<typename ...L>
	matrix(L... m) : data_{m...} {}	// variadic constructor for defining a matrix with better simplicity

	matrix(const matrix<elementType,rows_,cols_> &cpmat){ //copy constructor
		for(unsigned i=0;i<rows_;i++){
			for(unsigned j=0;j<cols_;j++){
				data_[i*cols_+j]=cpmat(i,j);
			}
		}
	}

	unsigned int numrows() const{
		return rows;
	}

	unsigned int numcols() const{
		return cols;
	}

	elementType& operator() (unsigned row, unsigned col) // () operator used as subscript
	{
	  return data_[cols_*row + col];
	}
	
	const elementType& operator() (unsigned row, unsigned col) const
	{
	  return data_[cols_*row + col];
	}

	template<class DERIVED>
	matrix& operator =(const Exp<DERIVED>& rhs){
		const DERIVED & mat = rhs.self();
		for(unsigned i=0;i<rows_;++i){
			for(unsigned j=0;j<cols_;++j){
				data_[cols_*i + j] = mat(i,j);
			}
		}
		return *this;
	}

//	addition assignment
	template<class DERIVED>
	matrix& operator +=(const Exp<DERIVED> & rhs){
		const DERIVED & mat = rhs.self();
		for(unsigned i=0;i<rows_;++i){
			for(unsigned j=0;j<cols_;++j){
				data_[cols_*i + j] = data_[cols_*i + j] + mat(i,j);
			}
		}
		return *this;
	}
	
//	multiplication assignment
	template<class DERIVED>
	matrix& operator *=(const Exp<DERIVED> & rhs){
		const DERIVED & mat = rhs.self();
		for(unsigned i=0;i<rows_;++i){
			for(unsigned j=0;j<cols_;++j){
				data_[cols_*i + j] = data_[cols_*i + j] * mat(i,j);
			}
		}
		return *this;
	}

//	subtraction assignment
	template<class DERIVED>
	matrix& operator -=(const Exp<DERIVED> & rhs){
		const DERIVED & mat = rhs.self();
		for(unsigned i=0;i<rows_;++i){
			for(unsigned j=0;j<cols_;++j){
				data_[cols_*i + j] = data_[cols_*i + j] - mat(i,j);
			}
		}
		return *this;
	}
	typedef const matrix& storeType;
	static const unsigned rows = rows_,cols=cols_;
	typedef elementType elementType_;


private:
	elementType data_[rows_*cols_];
 };


// class for adding two matrices or any other objects created using matrix expressions 

template<class left , class right>
class addExp: public Exp< addExp<left,right> >{
public:
	typedef const addExp& storeType;
	static const unsigned rows = left::rows,cols=right::cols;
	typedef typename left::elementType_ elementType_;
  	
  	addExp(const left& a, const right& b): op1(a), op2(b){}

	inline typename left::elementType_ operator()(const std::size_t i,const std::size_t j) const{ // () operator used as subscript
		return op1(i,j) + op2(i,j); 
	}

private:
	typename left::storeType op1; //operand 1 or LHS
	typename right::storeType op2;	//operand 2 or RHS
};

// function template for matrix addition
template<class Lhs, class Rhs>
inline addExp<Lhs, Rhs>
operator+(const Exp<Lhs> &lhs, const Exp<Rhs> &rhs) {
  return addExp<Lhs, Rhs>(lhs.self(), rhs.self());
}

// Matrix multiplication
template<class left , class right>
class multExp: public Exp< multExp<left,right> >{
public:
	typedef const multExp& storeType;
	static const unsigned rows = left::rows,cols=right::cols;
	typedef typename left::elementType_ elementType_;

  	multExp(const left& a, const right& b): op1(a), op2(b){}

	inline typename left::elementType_ operator()(const std::size_t i,const std::size_t j) const{ 
		typename left::elementType_ result = 0;
		for(unsigned k=0;k<left::cols;k++)
			result+= op1(i,k) * op2(k,j);
		return result; 
	}

private:
	typename left::storeType op1;
	typename right::storeType op2;
};

template<class Lhs, class Rhs>
inline multExp<Lhs, Rhs>
operator*(const Exp<Lhs> &lhs, const Exp<Rhs> &rhs) {
  return multExp<Lhs, Rhs>(lhs.self(), rhs.self());
}

// Matrix subtraction
template<class left , class right>
class subExp: public Exp< subExp<left,right> >{
public:
	typedef const subExp& storeType;
	static const unsigned rows = left::rows,cols=right::cols;
	typedef typename left::elementType_ elementType_;
  	
  	subExp(const left& a, const right& b): op1(a), op2(b){}

	inline auto operator()(const std::size_t i,const std::size_t j) const{ 
		return op1(i,j) - op2(i,j); 
	}

private:
	typename left::storeType op1;
	typename right::storeType op2;
};

template<class Lhs, class Rhs>
inline subExp<Lhs, Rhs>
operator-(const Exp<Lhs> &lhs, const Exp<Rhs> &rhs) {
  return subExp<Lhs, Rhs>(lhs.self(), rhs.self());
}

#endif
